
/* JavaScript content from js/graph.js in folder common */
/**
 * 
 */
      var dataStore = [];
      var t = new Date();
      for (var i = 9; i >= 0; i--) {
        var x = new Date(t.getTime() - i * 1000);
        dataStore.push([x, 0]);
      }

      var g = new Dygraph(document.getElementById("div_g"), dataStore,
                          {
                            drawPoints: true,
                            valueRange: [400, 2000],
                            colors: ["white"],
                            ylabel: 'Air Quality (VOC)',
                            axisLabelColor: "white",
                            axisLineColor: "white",
                              axisLineWidth: 2,
                              pointSize:5,
                              strokeWidth:3,
                              drawXGrid: false,
                              labels: ['Time', 'VOC']
                          });
      // It sucks that these things aren't objects, and we need to store state in window.
     window.intervalId = setInterval(function() {
        var x = new Date();  // current time
        var y = getAirQualityValue();
        var timeTmp;
        dataStore.push([x, y]);
        dataStore.splice(0,1);
        g.updateOptions( { 'file': dataStore } );
        //Updating time stamp
        timeTmp = x.toISOString();
        timeTmp = timeTmp.replace("T"," ");
        timeTmp = timeTmp.replace("Z","");
		var timeTag = document.getElementById("time");
		timeTag.innerHTML = timeTmp;
      }, 1000);