
/* JavaScript content from js/checkForCommands.js in folder common */
/**
 * 
 */

function subscribe(sendVOCform){
	
	WL.Logger.debug("Sending RT data for next 1 minute");
	document.getElementById("connect").host.value = sendVOCform.hostVOC.value;
	connect(document.getElementById("connect"));
	
	document.getElementById("subscribeTopicName").topicName.value = sendVOCform.topicNameCommands.value;
	
};

function checkCommand (sendVOCform){
	var command = document.getElementById("sendReceiveContent").receivedMessage.value;
	
	if (command == "SEND"){
		sendRTdata(sendVOCform);
	}
};