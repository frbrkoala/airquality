
/* JavaScript content from js/sendRTdata.js in folder common */
/**
 * 
 */

function sendRTdata(sendVOCform){
	
	WL.Logger.debug("Sending RT data for next 1 minute");
	var connectForm = document.getElementById("connect");
	
	if (connectForm.conn.disabled == ""){
		connectForm.host.value = sendVOCform.hostVOC.value;
		connectForm.clientId.value = sendVOCform.clientIdVOC.value;
		connectForm.userName.value = sendVOCform.userNameVOC.value;
		connectForm.password.value = sendVOCform.passwordVOC.value;
		
		document.getElementById("textMessageToSend").innerHTML = JSONmessage();
		document.getElementById("sendReceive").topicName.value = sendVOCform.topicNameVOC.value;
		
		connect(document.getElementById("connect"));
	}
	
	var startTime = Date.now();
	var currTime = startTime;
	var msgSentTime = startTime;
	
	while (currTime < startTime + 1200000) {
	
		if (currTime >= msgSentTime + 5000){
			var message = document.getElementById("textMessageToSend");
			message.innerHTML = JSONmessage();
			
			sendCurrentAQ();
			msgSentTime  = Date.now();
		}
	
		currTime = Date.now();
	}	
};