
/* JavaScript content from js/MQTTSample.js in folder common */
/*
============================================================================ 
Licensed Materials - Property of IBM

5747-SM3
 
(C) Copyright IBM Corp. 1999, 2012 All Rights Reserved.
 
US Government Users Restricted Rights - Use, duplication or
disclosure restricted by GSA ADP Schedule Contract with
IBM Corp.
============================================================================
 */

/**
 * @overview Sample Mobile application to send and receive MQTT messages 
 * @name MQTTSample
 */

// Initialisation entry point (called by Worklight)
//function wlCommonInit(){
//	setupApp();
//}

//function wlEnvInit(){
//	setupApp();
//}

/** MQTT API. */
var client;
var connectOptions;

/** The last message received. */
var receivedMessage;

/** Is Menu Visible **/
var isMenuVisible  =  false;

/**
 * Function that is called when the end-user presses the Connect button
 * 
 * @param {Object}
 *            The UI form containing connection options requested by the end-user
 */
function connect(form) {
	WL.Logger.debug("connect clicked");

	// Create a Messaging.Client object. This represents a connection to an MQTT server.
	try {
		client = new Messaging.Client(form.host.value, Number(form.port.value), form.clientId.value);

		// If the Client object has generated a new client id for us, then update the value shown in the UI
		if (form.clientId.value == "")
	  	  form.clientId.value = client.clientId; 
	} catch (exception) {
		WL.Logger.error("Exception creating client", exception);
		alert("Exception creating client");
	}

	// Set up Connection Options, using values from the connect form.
	var connectOptions = new Object();

	connectOptions.cleanSession = form.cleanSession.checked;
	connectOptions.keepAliveInterval = parseInt(form.keepAlive.value);

	if (form.userName.value && form.userName.value != "")
		connectOptions.userName = form.userName.value;
	if (form.password.value)
		connectOptions.password = form.password.value;

	// Add "Last Will and Testament" options, if a LWT message has been requested
	if (form.connectLWT.checked) {
		willMessage = new Messaging.Message(form.connectLWTMessage.value);
		willMessage.destinationName = form.connectLWTTopicName.value;
		willMessage.qos = parseInt(form.connectLWTQos.value);
		willMessage.retained = form.connectLWTRetained.checked;
		connectOptions.willMessage = willMessage;
	}

	// Set up callback functions to be notified when the connect call completes
	connectOptions.onSuccess = function() {
	//    connectForms();
	    onConnect();
	    log("Connected - id is " + client.clientId);
	};
	connectOptions.onFailure = onDisconnect;

	// Set up the Client-wide callback handlers
	client.onConnectionLost = onDisconnect;
	client.onMessageArrived = onMessageArrived;
	client.onMessageDelivered = function(message) {
		log("Sent msg " + message.payloadString);
	};
	
	// Enable tracing of the API implementation
	client.trace = function(msg) {
		if (msg.severity == "debug")
			WL.Logger.debug(msg.message);
		else
			WL.Logger.error(msg.message);
	};
	
	// Now make the actual connection
	client.connect(connectOptions);
} 

/**
 * Function that is called when the end-user presses the Disconnect button
 * 
 */
function disconnect() {
	WL.Logger.debug("disconnect clicked");
    client.disconnect(); // The connectionLost callback is driven when the connection closes.
}

/**
 * Function that is called when the end-user presses the Subscribe button
 * 
 * @param {Object}
 *            The UI form containing subscribe options requested by the end-user
 */
      
function subscribe(form) {
	WL.Logger.debug("subscribe clicked");
	var topicFilter = form.subscribeTopicName.value;
	var options = {qos:Number(form.requestedQos.value), 
				   invocationContext:{filter: topicFilter},
			       onSuccess:subscribeSucceeded, 
			       onFailure:subscribeFailed};
	
	try {
		client.subscribe(topicFilter, options);
		
	} catch (exception) {
		WL.Logger.error("Exception creating subscription", exception);
		alert("Subscribe to " + topicFilter + " failed");
	}
}

function subscribeSucceeded(result) {
	log ("Subscribed to " + result.invocationContext.filter);
	// Save topic for the next time the user runs the app
    localStorage.subTopic = result.invocationContext.filter;
}

function subscribeFailed(result) {
	log ("Subscribe to " + result.invocationContext.filter + " failed");
}

/**
 * Function that is called when the end-user presses the Unsubscribe button
 * 
 * @param {Object}
 *            The UI form containing the unsubscribe options requested by the end-user
 */

function unsubscribe(form) { 
	WL.Logger.debug("unsubscribe clicked");
	var topicFilter = form.subscribeTopicName.value;
	
	var options = { invocationContext:{filter: topicFilter},
					onSuccess: function(result){log ("Unsubscribed from " + result.invocationContext.filter);}, 
					onFailure: function(result){log ("Unsubscribe from " + result.invocationContext.filter + " failed");}};

	try {  
		client.unsubscribe(topicFilter, options);
		
	} catch (exception) {
		WL.Logger.error("Exception unsubscribing", exception);
		alert("Unsubscribe from " + topicFilter + " failed");
	}
}

function unsubscribeSucceeded(result) {
	log ("Unsubscribed from " + result.invocationContext.filter);
}

function unsubscribeFailed(result) {
	log ("Unsubscribe from " + result.invocationContext.filter + " failed");
}

/**
 * Function that is called when the end-user presses the Send button
 * 
 * @param {Object}
 *            The UI form containing contents of the message to be sent
 */
function doSend(form) {
	WL.Logger.debug("send clicked");
    //if (form.textMessage.value == "") {
    //    message = new Messaging.Message("");
    
    //} else if (form.messageType && form.messageType.value === "HEX") {
     //   var hex = form.textMessage.value;        
     //   var buffer = new ArrayBuffer((hex.length)/2);
     //	var byteStream = new Uint8Array(buffer); 
     //	var i = 0;	
     //	while (hex.length >= 2) { 
     //     var x = parseInt(hex.substring(0, 2), 16);
     //     hex = hex.substring(2, hex.length);
     //     byteStream[i++] = x;
     //   }
     //   message = new Messaging.Message(buffer);
    
    //} else
        message = new Messaging.Message(form.textMessage.value);
    
    message.destinationName = form.topicName.value;
    message.qos = parseInt(form.sendQos.value);  
  
    if (form.retained.checked) 
      message.retained = true;
    client.send(message);
    
    // Save the topic name for the next time the app is run
    localStorage.topic = form.topicName.value;
}


/**
 *  Client-level API callbacks.
 */
function onConnect() {	
    connectForms();
    sendCurrentAQ();
    log("Connected");
}

function onDisconnect(reason) {
	WL.Logger.debug("connection lost >> reason: "+ reason.errorMessage);
    disconnectForms();
    isDisconnectedAQ();
    // If normal disconnect
  
    if (reason.errorMessage == undefined)
      log ("Disconnected");
    else
      log("Unexpected disconnect " + reason.errorMessage);
}

function onMessageArrived(message) {
	WL.Logger.debug("message arrived");
    this.receivedMessage = message;
    displayMessage(message);
    log("Received " + message.payloadString);
}

/**
 *  Functions used to format messages when displayed in the UI
 */

function displayMessage(message) {
    this.receivedMessage = message;
 
    var form = document.getElementById("sendReceive");
    form.receivedTopicName.value = message.destinationName;
    form.receivedQos.value = message.qos;
    form.receivedRetainedCheckbox.checked = message.retained;
    form.receivedDuplicateCheckbox.checked = message.duplicate;
    
    if(form.displayFormat.value === "STRING") {
      form.receivedMessage.value = message.payloadString;
    } else {       
      var text = "";
      var textAscii = "";
      var messageBytes = message.payloadBytes;
      for (var i=0;i<messageBytes.length;i++) {
        if (messageBytes[i] <= 0xF)
          text = text+"0"+messageBytes[i].toString(16);
        else 
          text = text+messageBytes[i].toString(16);
    	if (messageBytes[i] >= 0x20 && messageBytes[i] <= 0x7E)
    	  textAscii = textAscii + String.fromCharCode(messageBytes[i]);
    	else 
    	  textAscii = textAscii + ".";	

        if (i%16 == 15) {
          text = text+" *"+textAscii+"*\n";
          textAscii = "";
        } else {
          text = text+" ";
        }
      }
      
      var padding = messageBytes.length%16;
      if (padding != 0) {
        var paddingString = "                ".substring(0,16-padding);
        text = text+paddingString+paddingString+paddingString;  
        text = text+"*"+textAscii+paddingString+"*";
      }
      form.receivedMessage.value = text;
    }   
}

function checkForHex(textbox, event) {
    var e = event || window.event;
  
    if (e.keyCode)
        code = e.keyCode;

    else if (e.which) 
        code = e.which;

    var character = String.fromCharCode(code);

    if (!e.ctrlKey && code!=9 && code!=8) {
        if (character.match(/[abcdefABCDEF1234567890]/g)) {
            return true;
        } else {
            return false;
        } 
    }
}

/** 
 * Function called when the user changes the received message displayType.
 */
function setReceivedMessageDisplayType(form) {
    if (receivedMessage)
      displayMessage(receivedMessage);
}

/** 
 * Function called when the user changes the outgoing messageType.
 */
function setMessageType(form) {  
    if (form.messageType.value === "STRING") {
      form.textMessage.onkeypress="";
    } else {
      form.textMessage.onkeypress="return checkForHex(this, event);";     
    }
}


/**
 * HTML Application logic.
 */
function setupApp() {

	// Initialise the UI for disconnected state
	disconnectForms();

	var form = document.getElementById("connect");    

	// Restore the values that were used last time
	form.host.value = localStorage.hostname || "";
	form.port.value = localStorage.port || 1883;
	form.clientId.value = localStorage.clientId || "";
	
	//Restore values for form on the AirQuality screen
	var VOCform = document.getElementById("sendVOCform");
	
	VOCform.hostVOC.value = localStorage.hostnameVOC || "";
	VOCform.topicNameVOC.value = localStorage.topicNameVOC || "";

	var cleanSession = localStorage.cleanSession;
	form.cleanSession.checked = ((cleanSession == undefined) ? true : cleanSession);
	form.userName.value = localStorage.userName || "";

	//-------------------------------------------------------------------
	// Tab Bar
	//-------------------------------------------------------------------

	WL.TabBar.init();
	
	WL.TabBar.addItem("airQualityTab", function(){ selectTab(0); },"Air Quality",{});

	WL.TabBar.addItem("connectionTab", function(){ selectTab(1); },"Server Conn.",{});
	WL.TabBar.addItem("sendReceiveTab", function(){ selectTab(2); },"Config. MQTT",{});
	WL.TabBar.addItem("historyTab", function(){ selectTab(3); },"Message Log",{});

	WL.TabBar.setVisible(false);
	selectTab(0);
	
}

/**
 * Display a tab.
 * 
 * @param tab		0=connect, 1=send/receive, 2=history
 */
function selectTab(tab) {
	WL.Logger.debug("tabClicked >> tab :: " + tab);
	
	document.getElementById("airQuality").style.display = "none";
	
	document.getElementById("connectContent").style.display = "none";
	document.getElementById("sendReceiveContent").style.display = "none";
	document.getElementById("historyContent").style.display = "none";

	if (tab === 0) {
		document.getElementById("airQuality").style.display = "block";
	}
	else if (tab === 1) {
		document.getElementById("connectContent").style.display = "block";		
	}
	else if (tab === 2) {
		document.getElementById("sendReceiveContent").style.display = "block";				
	}
	else if (tab === 3) {
		document.getElementById("historyContent").style.display = "block";				
	}
}

//-------------------------------------------------------------------
// Message Menu
//-------------------------------------------------------------------

function displayMenu() {
	WL.OptionsMenu.init();
	
//	WL.OptionsMenu.addItem("fireMsg", function(){ sendFireMsg(); },"Send Fire Message",{});
//	WL.OptionsMenu.addItem("transportMsg", function(){ sendTransportMsg(); },"Send Transport Message",{});
	//WL.TabBar.setVisible(true);
}

function showOrHideMenu(){
	//var visible = WL.TabBar.isVisible;
	WL.TabBar.setVisible(!isMenuVisible);
	
	//if (isMenuVisible){
	//	var sendVOCFormTmp = document.getElementById("sendVOCform");
	//	sendVOCFormTmp.style.display = "none";	
	//} else {
	//	var sendVOCFormTmp = document.getElementById("sendVOCform");
	//	sendVOCFormTmp.style.display = "block";
	//}

	isMenuVisible = !isMenuVisible;
}

//function sendFireMsg() {
	
//}

//function sendTransportMsg() {
	
//}

/**
 * Function to update the UI when a connection to the server has been established.
 * This function disables controls that allow connection attributes to be altered, and 
 * enables controls for subscribing, send messages and disconnecting
 */
// On connection
function connectForms() {  
    
    form = document.getElementById('connect');
    form.host.disabled = "true";
    form.port.disabled = "true"; 
    form.clientId.disabled = "true";
    form.userName.disabled = "true";
    form.password.disabled = "true";
    form.cleanSession.disabled = "true";
    form.keepAlive.disabled = "true";  
    
    form.connectLWTTopicName.disabled = "true";
    form.connectLWTMessage.disabled = "true";
    form.connectLWTQos.disabled = "true";
    form.connectLWTRetained.disabled = "true";
  
    form.conn.disabled = "true";
    form.disc.disabled = "";
    
    // Save values so that they are there next time the user runs the app
    localStorage.hostname = form.host.value;
    localStorage.port = form.port.value;
    localStorage.clientId = form.clientId.value;
    localStorage.cleanSession = form.cleanSession.checked;
    localStorage.userName = form.userName.value;
    
    form = document.getElementById('sendReceive');
    form.subscribeTopicName.disabled = "";
    form.subscribeButton.disabled = "";
    form.unSubscribeButton.disabled = "";
    form.topicName.disabled = "";
    form.textMessage.disabled = "";
    form.sendButton.disabled = "";
    
    // Restore the last topic names that were used successfully
    form.topicName.value = localStorage.topic || "";
    form.subscribeTopicName.value = localStorage.subTopic || "";
    form.requestedQos.value = 2;
    
    //Save values for VOC form
    var VOCform = document.getElementById("sendVOCform");
    localStorage.hostnameVOC = VOCform.hostVOC.value;
	localStorage.topicNameVOC = VOCform.topicNameVOC.value;
    
}

// On disconnection 
function disconnectForms(histclose) {  
      
    form = document.getElementById("connect");    
    form.host.disabled = ""; 
    form.port.disabled = "";  
    form.clientId.disabled = "";
    form.userName.disabled = "";
    form.password.disabled = "";
    form.cleanSession.disabled = "";
    form.keepAlive.disabled = "";
    
    form.connectLWTTopicName.disabled = "";
    form.connectLWTMessage.disabled = "";
    form.connectLWTQos.disabled = "";
    form.connectLWTRetained.disabled = "";
    
    form.conn.disabled = "";
    form.disc.disabled = "true";

    form = document.getElementById('sendReceive');
    form.subscribeTopicName.disabled = "true";
    form.subscribeButton.disabled = "true";
    form.unSubscribeButton.disabled = "true";
   
    form.topicName.disabled = "true";
    form.textMessage.disabled = "true";
    form.sendButton.disabled = "true";
}

/**
 * Function called when user checks or unchecks the LWT option 
 */ 

function setConnectLWT(form) { 
	if (form.connectLWT.checked) {
		document.getElementById("connectLWT").style.display = "";
	} else  {   
		document.getElementById("connectLWT").style.display = "none"; 
	}
}

/**
 * Function to write a line of text to the History window 
 */ 

function log(text) {   
    var form = document.getElementById('history');  
    var date = new Date();
    var h = padTime(date.getHours());
    var m = padTime(date.getMinutes());
    var s = padTime(date.getSeconds());
    var time = h + ":" + m + ":" + s + " "; 
  
    form.hist.value = form.hist.value + "\n" + time + text;
    form.hist.scrollIntoView(false);
}

function padTime(t) {
	if (t < 10) return "0"+t;
	else return t;
}

/** 
 * Function called when the user presses clear on the History page
 */

function logClear(form) {
	WL.Logger.debug("clear history clicked");
	form.hist.value = "";
}
