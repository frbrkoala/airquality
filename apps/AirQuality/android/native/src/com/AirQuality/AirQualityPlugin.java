package com.AirQuality;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class AirQualityPlugin extends CordovaPlugin {
		
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) 
			throws JSONException {
		
		if (action.equals("read")){
			
	//			String responseText = "Hello world, " + args.getString(0);
				callbackContext.success(AirQuality.vocString);
			
			return true;
		}
		
		return false;
	}

	
}