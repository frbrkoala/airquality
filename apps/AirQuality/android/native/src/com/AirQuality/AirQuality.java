package com.AirQuality;

import android.os.Bundle;

import com.worklight.androidgap.WLDroidGap;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.widget.TextView;

public class AirQuality extends WLDroidGap implements Runnable {
	
	public final static String EXTRA_MESSAGE = 
			"com.example.airquality.MESSAGE";
	public final static String EXTRA_CANCEL_MESSAGE = 
			"com.example.airquality.CANCEL_MESSAGE";

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    
	TextView textView;
	TextView debugView;

	final int vendorId = 0x03eb;
	final int productId = 0x2013;

	private UsbManager mUsbManager;
	private UsbDevice mDevice;
	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointIntrIn;
	private UsbEndpoint mEndpointIntrOut;
	PendingIntent mPermissionIntent;
	
	public static String vocString = "-1";
	
    float bar = 0f;
    
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

		mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(
				ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		registerReceiver(mUsbReceiver, filter);

		HashMap<String, UsbDevice> mDeviceList = mUsbManager.getDeviceList();
		Iterator<UsbDevice> mDeviceIterator = mDeviceList.values().iterator();
		
		/*
		textView = (TextView) findViewById(R.id.USBdevices);
		textView.setMovementMethod(new ScrollingMovementMethod());
		textView.setText("No Device");
		*/
		while (mDeviceIterator.hasNext()) {
			mDevice = mDeviceIterator.next();

			if ((mDevice.getVendorId() == vendorId)
					&& (mDevice.getProductId() == productId)) {
			
				mUsbManager.requestPermission(mDevice, mPermissionIntent);
				 				
				break;
			}
		}				

		
    }
  
    @Override
	public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        unregisterReceiver(mUsbReceiver);
       
 
    }

    @Override
	public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		registerReceiver(mUsbReceiver, filter);                
    }    
        
	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					UsbDevice device = (UsbDevice) intent
							.getParcelableExtra(UsbManager.EXTRA_DEVICE);

					if (intent.getBooleanExtra(
							UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						if (device != null) {
							setDevice(device);
						}
					} else {
						//textView.append("permission denied for device "
						//		+ device);
					}
				}
			}
		}
						
	};	     
 
	private void setDevice(UsbDevice device) {
		
		if (device.getInterfaceCount() != 1) {
			
			return;
		}
		UsbInterface intf = device.getInterface(0);
		// device should have one endpoint
		if (intf.getEndpointCount() != 2) {
			
			return;
		}
		// endpoint should be of type interrupt
		UsbEndpoint ep0 = intf.getEndpoint(0);
		UsbEndpoint ep1 = intf.getEndpoint(1);

		if ((ep0.getType() != UsbConstants.USB_ENDPOINT_XFER_INT)
				|| (ep1.getType() != UsbConstants.USB_ENDPOINT_XFER_INT)) {
			return;
		}

		if (ep0.getDirection() == UsbConstants.USB_DIR_IN) {
			mEndpointIntrIn = ep0;
			mEndpointIntrOut = ep1;
		} else {
			mEndpointIntrIn = ep1;
			mEndpointIntrOut = ep0;
		}

		if (device != null) {
			UsbDeviceConnection connection = mUsbManager.openDevice(device);
			
			if (connection != null && connection.claimInterface(intf, true)) {

				mConnection = connection;
				Thread thread = new Thread(this);
				thread.start();

			} else {
				
				mConnection = null;
				
			}
		}
	}


	@Override
	public void run() {
		
		//Basically, you have to poll the stick.  You send something in the bufferOut output buffer, and get back a response back in the bufferIn input buffer.
		
		ByteBuffer bufferOut = ByteBuffer.allocate(16);
		ByteBuffer bufferIn = ByteBuffer.allocate(16);

		byte b[] = { 0x40, 0x68, 0x2a, 0x54, 0x52, 0x0a, 0x40, 0x40, 0x40,
				0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40 };
		bufferOut.put(b);

		UsbRequest requestIn = new UsbRequest();
		UsbRequest requestOut = new UsbRequest();

		requestIn.initialize(mConnection, mEndpointIntrIn);
		requestOut.initialize(mConnection, mEndpointIntrOut);
		
		requestIn.queue(bufferIn, 16);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		requestOut.queue(bufferOut, 16);
		Integer inCount = 0;
		while (true) {

					
			UsbRequest req = mConnection.requestWait();

			if (req == requestIn) {
				
				requestIn.queue(bufferIn, 16);
				inCount++;
				if (inCount == 2) {
					requestOut.queue(bufferOut, 16);
					inCount = 0;
				} else {
				
					short result = bufferIn.getShort(2);
					Short sh = Short.reverseBytes(result);
					
					if ((sh > 400) && (sh < 9000)) {
						
	// Getting value from the sensor into vocString, which is used in AirQualityPlugin 
	// which sends the value into JavaSacript through Cordova plugin
						
						vocString =  sh.toString();
						

						
					} else {
						inCount--;
					}
					
				}

			} else

			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
					
			
		}									
	}
   
	
	/**
     * onWLInitCompleted is called when the Worklight runtime framework initialization is complete
     */
	@Override
	public void onWLInitCompleted(Bundle savedInstanceState){
		super.loadUrl(getWebMainFilePath());
		// Add custom initialization code after this line
	}
}



