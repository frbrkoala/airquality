/**
 * This function will construct the string for CSV data source
 */

function CSVmessage (){
	//Getting timezone offset
	var date = new Date();
	var timezoneoffset = date.getTimezoneOffset();
	
	var vocValue = document.getElementById("aq").innerHTML;
	var timeValue = document.getElementById("time").innerHTML;
	
	var latitudeValue = document.getElementById("geolocationLatitude").innerHTML;
	var longitudeValue = document.getElementById("geolocationLongitude").innerHTML;
	
	return timeValue+","+timeValue+","+longitudeValue+","+latitudeValue+","+"AirQuality value "+vocValue+","+timeValue+","+timezoneoffset+",ADMINIB-J36RM99,Air Quality: "+vocValue+" voc";
};

function JSONmessage(){
	var date = new Date();
	var timezoneoffset = date.getTimezoneOffset();
	
	//var vocValue = document.getElementById("aq").innerHTML;
	//var timeValue = document.getElementById("time").innerHTML;
	
	var vocValue = getAirQualityValue();
	var timeValue = date;
	
	var latitudeValue = document.getElementById("geolocationLatitude").innerHTML;
	var longitudeValue = document.getElementById("geolocationLongitude").innerHTML;
	
	return JSON.stringify({d:{"name" : "Air Quality",
				"time" : timeValue , 
				"timeZoneOffset" : timezoneoffset,
				"longitude": longitudeValue,
				"latitude" : latitudeValue,
				"voc" : vocValue
			  }
		});
};