/**
 * Detecting location via PhoneGap plugin
 */

    document.addEventListener("deviceready", onDeviceReadyGeolocation, false);

    // Cordova is ready
    //
    function onDeviceReadyGeolocation() {
    	setInterval(function() {
        navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeolocation, { enableHighAccuracy: true });
    	},5000);
    }

    // onSuccess Geolocation
    //
    function onSuccessGeolocation(position) {
//        var element = document.getElementById('geolocation');
//        element.innerHTML = 'Latitude: '           + position.coords.latitude              + '<br />' +
//                            'Longitude: '          + position.coords.longitude             + '<br />' +
//                            'Altitude: '           + position.coords.altitude              + '<br />' +
//                            'Accuracy: '           + position.coords.accuracy              + '<br />' +
//                            'Altitude Accuracy: '  + position.coords.altitudeAccuracy      + '<br />' +
//                            'Heading: '            + position.coords.heading               + '<br />' +
//                            'Speed: '              + position.coords.speed                 + '<br />' +
//                            'Timestamp: '          + position.timestamp          + '<br />';
        document.getElementById('geolocationLatitude').innerHTML = position.coords.latitude;
        document.getElementById('geolocationLongitude').innerHTML = position.coords.longitude;
        document.getElementById('geolocationAccuracy').innerHTML = position.coords.accuracy;
    }

    // onError Callback receives a PositionError object
    //
    function onErrorGeolocation(error) {
        alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
    }