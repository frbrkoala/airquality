/**
 * This function will connect to MQTT server
 */

function connectForAQ(sendVOCform){
	
	WL.Logger.debug("connectForAQ() clicked");
	var connectForm = document.getElementById("connect");
	
	if (connectForm.conn.disabled == ""){
		connectForm.host.value = sendVOCform.hostVOC.value;
		connectForm.clientId.value = sendVOCform.clientIdVOC.value;
		connectForm.userName.value = sendVOCform.userNameVOC.value;
		connectForm.password.value = sendVOCform.passwordVOC.value;
		
		document.getElementById("textMessageToSend").innerHTML = JSONmessage();
		document.getElementById("sendReceive").topicName.value = sendVOCform.topicNameVOC.value;
		
		connect(document.getElementById("connect"));
	} else {
		sendCurrentAQ();
	}
	
};

/**
 * This function will send current VOC value through MQTT
 */

function sendCurrentAQ(){
	doSend(document.getElementById("sendReceive"));
	WL.Logger.debug("From sendCurrentVOC: Conection success and message sent!");
}

/**
 * This function will and here we are logging that we are now disconnected from MQTT server
 */

function isDisconnectedAQ(){
	WL.Logger.debug("From sendCurrentVOC: Disconnected");
}